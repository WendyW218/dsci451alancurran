#Yang HU
#3/16/2016
# calculate the uncertainty  of Rd 

uncertainty<- function(Rd){
  b = Rd$coefficients[1]
  m = Rd$coefficients[2]
  Db = coef(summary(Rd))[1,2]
  Dm = coef(summary(Rd))[2,2]
  DRd = sqrt(b^2*Dm^2+Db^2)/b^2
  names(DRd) = "Uncertainty"
  return (DRd)
}