
source("H:/Git/dsci451alancurran/code/asy-1701-inverter-saturation.R")
source("H:/Git/dsci451alancurran/code/asy_1701_data_filtering.R")
source("H:/Git/dsci451alancurran/code/asy-1701-predictive-model.R")

stats <- NULL
data <- read.csv("file:///V:/vuv-data/proj/BAPVC-TSA/SunPower/copy/data-cleaned/ldohpsi.csv")
data <- data_filter(data)
limit <- saturation_test(data)
if (!is.na(limit))
  data <- subset(data, AC_power <= limit)
sum_table <- predictive_model(data, limit = limit)
GHI <- round(sum_table$GHI[1], 2)
temp <- round(sum_table$Ambient_temp[1], 2)
ws <- round(sum_table$Wind_speed[1], 2)
wghts <- 1/sum_table$sigma
Rd <- lm(power_predict ~ Age, weights = wghts, data = sum_table)
title = paste("at GHI", GHI, "w/m^2,", "ambient temp", temp, "C,", "wind speed", ws, "m/s" )
plot(data = sum_table, power_predict ~ Age, ylim = c(0, max(power_predict, na.rm = TRUE)*1.1),xlab = "Age of the system (pseudo-months)", ylab = "Predicted AC power (kW)", main = title)
arrows(sum_table$Age, sum_table$power_predict - sum_table$sigma, sum_table$Age, sum_table$power_predict + sum_table$sigma, length = 0.03, angle = 90, code = 3)
abline(Rd, col = "blue")

Rc <- Rd$coefficients[[2]]/Rd$coefficients[[1]]
stat_sum <- c(i, Rc, summary(Rd)$adj.r.squared)
stats <- rbind(stats, stat_sum)