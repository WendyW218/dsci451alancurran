#Alan Curran    
#10/17/2016

#this script finds data set mismatches between the SunPower metadata and actual data
#by comparing the number of inverters in each data file

library(dplyr)

setwd("V:/vuv-data/proj/BAPVC-TSA/SunPower")
metadata <- read.csv("SunPower_modules_subset_modified.csv", colClasses = c(NA,NA,"NULL","NULL",
                                                                            "NULL","NULL","NULL",
                                                                            "NULL","NULL","NULL",
                                                                            "NULL","NULL","NULL",
                                                                            "NULL","NULL","NULL",
                                                                            "NULL","NULL"))
metadata[,1] <- gsub("$",".csv",metadata[,1])
meta_files <- unique(metadata$Project..)

data_files <- list.files(path = "./copy/data-csv")

meta_match <- meta_files[meta_files %in% data_files]
meta_mismatch <- meta_files[!meta_files %in% data_files]

data_names <- read.csv("sunpower_data_variable_names.csv")
data_names <- data_names[-1]

#correct for a data file with missing columns
data_names[134,c(4,5)] <- "None"
data_names[134,c(6)] <- "InverterPower"

inv_data_names <- data_names[-c(2:5)]

inv_meta_data <- NULL
for (i in meta_match){
  inv_names <- data.frame(i, metadata$Project.Name[metadata$Project.. == i])
  
  
}




