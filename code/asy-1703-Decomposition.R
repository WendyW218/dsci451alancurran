
# install.packages("stlplus")
# install.packages("padr")
library(stlplus)
library(padr)
library(ggplot2)

#read files containing the beta model predicted power results
files <- list.files("~/git/dsci451alancurran/topics", pattern = "PVSC_")

for (i in files) {
  #i = "61ckntu_summary.csv"
  file.key <- gsub("1705_PVSC_", "", i)
  file.key <- gsub(".csv", "", file.key)
  #read in predicted power files
  sum.data <- read.csv(paste("~/git/dsci451alancurran/topics/", i, sep = ""))
  #remove any zero error months that we not filtered out, this data was interpolated and must be removed
  sum.data <- subset(sum.data, r.squared != 1)
  #remove error months with low sigma values
  sum.data <- subset(sum.data, sigma >= 1e-3)
  total.age <- 1:max(sum.data$Age, na.rm = TRUE)
  power.sum <- as.data.frame(total.age)
  
  #add NA rows where there is no monthly data
  #collect the power and error data
  for (j in power.sum$total.age) {
    if (j %in% sum.data$Age) {
      power.sum$power[j] <- sum.data$power_predict[which(sum.data$Age == j)]
      power.sum$sigma[j] <- sum.data$sigma[which(sum.data$Age == j)]
    } else {
      power.sum$power[j] <- NA
      power.sum$sigma[j] <- NA
    }
  }
  
  #If data is missing at the start is NA, remove it and start from the existing values
  while (is.na(power.sum$power[1])) {
    power.sum <- power.sum[-1,]
  }
  #Same for NAs at the end of the time series
  while (is.na(power.sum$power[length(power.sum$power)])) {
    power.sum <- power.sum[-length(power.sum$power),]
  }
  
  #define time series object of the predicted power, frequency is 12 months/year
  ts.power <- ts(power.sum$power, frequency = 12)
  #stlplus to decompose with NA months
  stl.model <- stlplus(x = ts.power, s.window = "periodic")
  stl.data <- stl.model[[1]]
  #define column showing if trend month was interpolated from an NA month
  stl.data$interpolated <- ifelse(is.na(stl.data$raw), TRUE, FALSE)
  stl.data$age <- power.sum$total.age[1]:tail(power.sum$total.age, n = 1)
  stl.data$sigma <- power.sum$sigma
  stl.data$operating <- 1:length(stl.data$age)
  
  title <- paste(file.key, "at GHI", round(sum.data$GHI[1], 2), "(W/m^2),", "ambient temp", round(sum.data$Ambient_temp[1], 2), "(C),", "wind speed", round(sum.data$Wind_speed[1], 2), "(m/s)")
  
  png(file = paste("~/trend/figs/1705_trend_PVSC_", file.key, ".png", sep = ""))
  #plot the trend of the predicted power, color by if the month had raw data, add sigma error bar
  plot(stl.data$operating, 
       stl.data$trend, 
       col = ifelse(stl.data$interpolated == TRUE,'green','blue'), 
       ylim = c(0,1.1*max(stl.data$trend)),
       xlab = "Age (Pseudo Months)",
       ylab = "Predicted Power (KW) Trend",
       main = title)
  arrows(stl.data$operating, 
         stl.data$trend - stl.data$sigma, 
         stl.data$operating, 
         stl.data$trend + stl.data$sigma, 
         length = 0.03, 
         angle = 90, 
         code = 3,
         col = "blue")
  
  stl.data$power <- ifelse(stl.data$interpolated == TRUE, NA, stl.data$trend)
  #define power change trendline
  Rd <- lm(power ~ operating, data = stl.data, weights = 1/sigma)
  abline(Rd)
  
  dev.off()
  
  #change rate (%/yr) is defined as slope/intercept*12
  Rc <- summary(Rd)$coefficients[[2]]/summary(Rd)$coefficients[[1]]*12*100
  
  write.csv(stl.data, paste("~/trend/data/1705_trend_PVSC", file.key, ".csv", sep = ""))
  
  
}
