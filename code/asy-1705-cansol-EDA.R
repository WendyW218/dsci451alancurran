
library(data.table)
library(dplyr)
dat1 <- fread("/mnt/projects/CSE_MSE_RXF131/vuv-data/proj/CanadianSolar/RawData/P01-Inv_2.csv", stringsAsFactors = FALSE)
ws <- fread("/mnt/projects/CSE_MSE_RXF131/vuv-data/proj/CanadianSolar/RawData/P01-WS_1.csv", stringsAsFactors = FALSE)
test <- merge(x = dat1, y = ws)
hold <- test[,c(21,20,19,18,13)]

hold <- hold[complete.cases(hold),] 
hold <- subset(hold, InvKWDC != "null") 
hold <- data.frame(sapply(hold, as.numeric)) 
hold <- subset(hold, TiltedRadiation >= 50) 
hold <- subset(hold, InvKWDC >= 10) 
hold <- subset(hold, AirTemperature <= 40)
# Already seen that there is a temperature error, 1555 obs. at 144 Tmod and 80 Tair

library(psych)
pairs.panels(hold, ellipses = FALSE, smooth = FALSE)
