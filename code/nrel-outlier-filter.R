##yang hu
##04/13/2016
##outlier filter of responsivity
## throw away responsivity values that are far from the mean in each month
library(dplyr)
outlier_filter<-function(data, n=3){
  check<-group_by(data, age)
  
  table1<-dplyr::summarize(check, 
                           
                           count = n(),
                           res.mean = mean(responsivity, na.rm = TRUE),
                           res.median = median(responsivity, na.rm = TRUE),
                           res.sd = sd(responsivity, na.rm = TRUE)
  )
  data2<-data
  for (i in 1:nrow(table1)){
    if (table1$count[i]<20) data2<-data2[!(data2$age==table1$age[i]),]
    else {
      sub<-subset(data2,age == table1$age[i])
      sub<-subset(sub, (responsivity>(table1$res.mean[i]+n*table1$res.sd[i]))|(responsivity<(table1$res.mean[i]-n*table1$res.sd[i])))
      data2<-data2[!(data2$Timestamp %in%sub$Timestamp),]  
    }
  }
  res.mean.all <- mean(data$responsivity, na.rm = TRUE)
  res.median.all <- median(data$responsivity, na.rm = TRUE)
  res.sd.all <- sd(data$responsivity, na.rm = TRUE)
  sub<-subset(table1,(res.mean>(res.mean.all+3*res.sd.all))|(res.mean<(res.mean.all-3*res.sd.all)))
  data2<-data2[!(data2$age %in%sub$age),] 
  return(data2)
}