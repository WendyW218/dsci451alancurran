---
title: "Lab Excersize 2a"
author: "Alan J Curran"
date: "February 14th, 2017"
output: 
  pdf_document:
    toc: true
    number_sections: true
    toc_depth: 5
  html_document:
    toc: true
    number_sections: true
    toc_depth: 5
---

<!--
# Script Name: dsci453-curran-LE2a.Rmd
# Purpose: This is an Rmd file summarizing the final step of the DSCI451 semester project
# Authors: Alan J. Curran
# License: Creative Commons Attribution-ShareAlike 4.0 International License.
##########
# Latest Changelog Entires:
# v0.00.01 - dsci453-curran-LE2a.Rmd - Alan Curran Created this first draft 
##########

-->

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

 \setcounter{section}{1}
 \setcounter{subsection}{0}
 \setcounter{subsubsection}{0}



```{r, echo = FALSE}


download.file("http://www.openintro.org/stat/data/mlb11.RData", destfile = "mlb11.RData")
load("mlb11.RData")



plot(runs ~ at_bats, data = mlb11)
cat("The data seems linear, \nI would be comfortable using a linear model at this point.")



cor(mlb11$runs, mlb11$at_bats)


cat("More at_bats correlates to more runs.\n There is a lot of noise in the data.")


plot_ss(x = mlb11$at_bats, y = mlb11$runs)

# After running this command, you'll be prompted to click two points on the plot to define a line. Once you've done that, the line you #specified will be shown in black and the residuals in blue. Note that there are 30 residuals, one for each of the 30 observations. Recall #that the residuals are the difference between the observed values and the values predicted by the line:
#   
#   ei=yi-yhati    [This would be better in Rmd and using MathMode "$  ...  $" to typeset the equation]
# 
# The most common way to do linear regression is to select the line that minimizes the sum of squared residuals. To visualize the squared #residuals, you can rerun the plot command and add the argument showSquares = TRUE.

plot_ss(x = mlb11$at_bats, y = mlb11$runs, showSquares = TRUE)

# Note that the output from the plot_ss function provides you with the slope and intercept of your line as well as the sum of squares.
#
### 
# Exercise 3 
### 
# Using plot_ss, choose a line that does a good job of minimizing the sum of squares. Run the function several times. What was the smallest #sum of squares that you got? How does it compare to your neighbors?
#
cat("143637 was the lowest I was able to get")
## 
# The linear model
##
# It is rather cumbersome to try to get the correct least squares line, i.e. the line that minimizes the sum of squared residuals, through #trial and error. Instead we can use the lm function in R to fit the linear model (a.k.a. regression line).

m1 <- lm(runs ~ at_bats, data = mlb11)

# The first argument in the function lm is a formula that takes the form y~hlsymbolx. Here it can be read that we want to make a linear #model of runs as a function of at_bats. The second argument specifies that R should look in the mlb11 data frame to find the runs and #at_bats variables.
# 
# The output of lm is an object that contains all of the information we need about the linear model that was just fit. We can access this #information using the summary function.

summary(m1)

# Let's consider this output piece by piece. First, the formula used to describe the model is shown at the top. After the formula you find #the five-number summary of the residuals. The "Coefficients" table shown next is key; its first column displays the linear model's #y-intercept and the coefficient of at_bats. With this table, we can write down the least squares regression line for the linear model:
#   
#   yhat = -2789.2429 + 0.6305*atbats
# 
# One last piece of information we will discuss from the summary output is the Multiple R-squared, or more
# simply, R2. The R2 value represents the proportion of variability in the response variable that is explained
# by the explanatory variable. For this model, 37.3% of the variability in runs is explained by at-bats.
#
### 
# Exercise 4 
### 
# Fit a new model that uses homeruns to predict runs. Using the estimates from the R output, write the equation of the regression line. What does the slope tell us in the context of the relationship between success of a team and its home runs?
# 

model <- lm(runs ~ homeruns, data = mlb11)
summary(model)
cat("runs = 415.2 + 1.83*homeruns")
cat("The slope tell us that there is positive trend between runs and homeruns. \nMore homeruns = more runs")

## 
# Prediction and prediction errors
## 
# Let's create a scatterplot with the least squares line laid on top.

plot(mlb11$runs ~ mlb11$at_bats)
abline(m1)

# The function abline plots a line based on its slope and intercept. Here, we used a shortcut by providing the model m1, which contains #both parameter estimates. This line can be used to predict y at any value of x. When predictions are made for values of x that are beyond #the range of the observed data, it is referred to as extrapolation and is not usually recommended. However, predictions made within the #range of the data are more reliable. They're also used to compute the residuals.
#
### 
# Exercise 5 
### 
# If a team manager saw the least squares regression line and not the actual data, how many runs would he or she predict for a team with #5,578 at-bats? Is this an overestimate or an underestimate, and by how much? In other words, what is the residual for this prediction?
#
summary(m1)
-2789.24+0.6305*5578
cat("Based on the regression line, a coach would estimate ~728 runs from 5,578 at-bats")
mlb11[mlb11$at_bats == 5579, "runs"] - 728
cat("The results from the data are 15 runs below the estimation.\n The coach would have overestimated")

## 
# Model diagnostics
##
# To assess whether the linear model is reliable, we need to check for (1) linearity, (2) nearly normal residuals,
# and (3) constant variability.
# 
# 1. Linearity: You already checked if the relationship between runs and at-bats is linear using a scatterplot. We should also verify this #condition with a plot of the residuals vs. at-bats. Recall that any code following a # is intended to be a comment that helps understand #the code but is ignored by R.
# 
plot(m1$residuals ~ mlb11$at_bats)
abline(h = 0, lty = 3) # adds a horizontal dashed line at y = 0
#
### 
# Exercise 6 
### 
# Is there any apparent pattern in the residuals plot? What does this indicate about the linearity of the relationship between runs and #at-bats?
# 
cat("There does not appear to be a pattern in the residuals plot, \nthe data seems fairly linear")
# 2. Nearly normal residuals: To check this condition, we can look at a histogram, hist(m1$residuals), or a normal probability plot of the residuals.
hist(m1$residuals)
qqnorm(m1$residuals)
qqline(m1$residuals) # adds diagonal line to the normal prob plot

###
# Exercise 7 
###
# Based on the histogram and the normal probability plot, does the nearly normal residuals condition appear to be met?
# 
cat("The residuals are weighted towards the negative.\n This trend is not nearly normal.")
# 3. Constant variability:
#
###   
# Exercise 8 Based on the plot in (1), does the constant variability condition appear to be met?
### 
cat("The constant variability does not appear to be met,\n although it seems close.")
#
### 
# On Your Own
###
# Choose another traditional variable from mlb11 that you think might be a good predictor of runs. Produce a scatterplot of the two #variables and fit a linear model. At a glance, does there seem to be a linear relationship?
# How does this relationship compare to the relationship between runs and at_bats? Use the R2values from the two model summaries to #compare. Does your variable seem to predict runs better than at bats? How can you tell?

plot(mlb11$runs ~ mlb11$hits)
model <- lm(mlb11$runs ~ mlb11$hits)
summary(model)
abline(model, col = "blue")

cat("Initially the data appears linear. \nThe R2 of 0.6419 is much higher than the at_bat R2 of 0.3729.\n Both show a positive trend but hits appears to predict better (higher R2)")

# Now that you can summarize the linear relationship between two variables, investigate the relationships between runs and each of the #other five traditional variables. Which variable best predicts runs? Support your conclusion using the graphical and numerical methods #we've discussed (for the sake of conciseness, only include output for the best variable, not all five).

library(psych)
data_sub <- mlb11[,c(2,3,4,5,6,7,8,9)]
pairs.panels(data_sub, smooth = FALSE, ellipses = FALSE)

plot(mlb11$runs ~ mlb11$bat_avg)
model <- lm(mlb11$runs ~ mlb11$bat_avg)
abline(model, col = "blue")
summary(model)

cat("From the pairs plot it can be seen that bat_avg is the best predictor (highest correlation) for runs.\n The model has the highest seen R2 of 0.6561")

# Now examine the three newer variables. These are the statistics used by the author of Moneyball to predict a teams success. In general, #are they more or less effective at predicting runs that the old variables? Explain using appropriate graphical and numerical evidence. Of #all ten variables we've analyzed, which seems to be the best predictor of runs? Using the limited (or not so limited) information you know #about these baseball statistics, does your result make sense?

data_sub <- mlb11[,c(2,10,11,12)]
pairs.panels(data_sub, smooth = FALSE, ellipses = FALSE)
plot(mlb11$runs ~ mlb11$new_obs)
model <- lm(mlb11$runs ~ mlb11$new_obs)
abline(model, col = "blue")
summary(model)
cat("All three of these variables show a higher correlation than bat_avg (0.81).\n The highest of these is a correlation of 0.97 for new_obs with an R2 of 0.9349")
cat("It makes sense that there would be a strong correlation between \nthe ability to get onto a base and how hard one hits and scoring. \nI know very little about baseball so my personal opinion is not worth much, however.")
# Check the model diagnostics for the regression model with the variable you decided was the best predictor for runs.

hist(model$residuals)
qqnorm(model$residuals)
qqline(model$residuals)

cat("The linear trend appears to be very regular.")
# What concepts from the textbook are covered in this lab? What concepts, if any, are not covered in the textbook? Have you seen these #concepts elsewhere, e.g. lecture, discussion section, previous labs, or homework problems? Be specific in your answer.
# 
cat("General lab concepts were covered, such as fitting, correlation, and prediction for modeling.\n The only concept that was not covered was background information about baseball but it is probably better now knowing much about that going into this problem - \nlet the stats speak for themselves.")
# 
# 
# 
# 
# This is a product of OpenIntro that is released under a Creative Commons Attribution-ShareAlike 3.0 Unported
# (http://creativecommons.org/licenses/by-sa/3.0/). This lab was adapted for OpenIntro by Andrew Bray and Mine �etinkaya-Rundel
# from a lab written by the faculty and TAs of UCLA Statistics.
# 
# ???Though it's not necessary for this lab, if you'd like a refresher in the rules of baseball and a description of these statistics, visit
# http://en.wikipedia.org/wiki/Baseball_rules and http://en.wikipedia.org/wiki/Baseball_statistics.
```

<!--

# v0.00.01 - 1612-DSCI451-semproj4-curran.Rmd - Alan Curran created this first draft 

-->
