\documentclass[]{article}

%opening
\title{G/NG 4.1.1}
\author{}

\begin{document}

\maketitle

\begin{abstract}
	In accordance with the G/NG milestone two degradation rate calculations were applied to max power points from $I-V$ data streams in two Koppen-Geiger climate zones, BWh, and ET. 
	The first method, the Respnsivity Method, uses an assumed linear degradation to plot and filter the Performance Ratios of the time series to calculate a degradation rate $(R_d)$. 
	The second method, the Month-by-Month $(MbM)$ Method, builds a predictive model for each month and plots the results of those models as a predicted power time series, from which the $R_d$ is calculated. 
	The $MbM$ method was developed at the Solar Durability and Lifetime Extension (SDLE) Research Center as a more effective means of modeling the characteristics of a real-world system and does not assume a linear degradation rate when evaluating a system. 
	Results from the MbM method shows a non-linear trend for one of the sites and a linear trend for the other. 
A comparison to the raw data showed that the MbM method predicted the power of both systems with greater accuracy at ~3 and ~5 years.        
\end{abstract}

\section{Milestone 4.1.1, Go/No-Go Decision Point}
Stressor/response predictive models of features in outdoor I-V datastreams among at least two climate zones over five years for a single module brand that are time-dependent.  
We will show an improvement over the commonly used linear degradation rate (0.5-2\%/year) model by 5\% (relative) improvement at 3 and 5 years. 
Milestone Verification Process: Uncertainty on fitted time-coefficient excludes zero. 

\section{Introduction}

The purpose of this milestone was to compare the Month-by-Month (MbM) model to a commonly used linear degradation model to show the differences between the two. 
The Responsivity method developed by the National Renewable Energy Laboratory (NREL) is a widely used method for calculating the degradation rate of a PV system. 
It subsets the data into the ideal weather condidtions and uses a linear fit to model the change. 
This can be problematic as the process removes most of the data and removes data based on a deviation from a linear trend that may not be the best representation of the degradation. 
The MbM method attempts to improve this process by minimizing the amount of data cleaning and not coercing a linear fit to the degradation. 

\section{Methodology}

The MbM method and the Responsivity method were both performed on the same data set from Fraunhofer-ISE. 
The data set was two systems in two different climate zones (BWh, and ET) with max power data every few minutes dating back to 2011. 
The modules in both climate zones are the same brand and model. 

\subsection{The Month-by-Month Method}
The MbM method has already been discussed in previous reports so this will be a brief summary of the process. 
The data is sorted into 30 day long pseudo-months during which it is assumed that no degradation takes place. 
For each of the pseudo-months in the time series, a predictive model is developed which models the power as a function of the instantaneous weather conditions, including the irradiance, the module temperature, and the air mass. 
Once a model for each month has been determined, standard weather conditions of the average temperature, and average air mass, and the minimum of the max irradiance values from each month. 
The standard weather conditions are applied to all the models, yielding predicted power values for each month under the same weather conditions for the lifetime of the system. 
The a weighted linear regression model is fit to the trend of the monthly predicted power outputs which is used to determine the $R_c$ for the system. 
The $R_c$ for the system in \%/year is defined below as the slope divided by the y-intercept times 12. 
$$ \frac{Slope}{y-Intercept} * 12 $$ 
The MbM method can also fit a segmented regression model if the time series shows a non-linear trend. 
The segmented regression fits separate linear models between change points of the time series instead of assuming a constant linear trend over the lifetime of the system. 

\subsection{The Responsivity Method}

The Responsivity method begins by removing all the data that does not fall within a given rage of a predefined standard irradiance. 
The standard irradiance is usually $1000\ W/m^2$ but for the sake of comparing the two methods, the it was defined as the same standard irradiance found by the MbM method. 
Once the standard irradiance is determined, the data is subset to only the data that falls within $\pm\ 50\ W/m^2$ of the standard. 
The relative responsivity (shown below) is calculated for every remaining data point using a performance ratio with a temperature correction. 

$$Responsivity = \frac{Power_{observed}}{Irrad_{observed}} * \frac{Irrad_{standard}}{Power_{nameplate}}/[1 + \gamma(T_{measured} - T_{standard})] $$ 

The standard temperature $(T_{standard})$ is calculated by selecting the median temperature within the range of the standard irradiance. 
The $\gamma$ temperature correction coefficient is calculated empirically by plotting the performance ratio of first year of data as a function of temperature. 
Outliers are removed and the slope of the fit is the $\gamma$ coefficient. 
This process is repeated for the final year of data to determine if $\gamma$ changes with time. 
If it does a time dependent temperature correction is used. 
Once the responsivity values have been calculated they are plotted with respect to time and two filters are performed. 
The first filter removes outliers in increments of 100 points. 
The standard deviation threshold for removal at this point is a variable parameter. 
In this analysis data outside $0.5x$ the standard deviation was removed. 
The second filter removes outliers from the derivative of the responsivity trend. 
This filter is meant to eliminate nonlinear features from the time series. 
Once again the data outside of $0.5x$ of the standard deviation is removed.    

\subsection{Data Source and Cleaning}

The data for this project came from FraunhoferISE. 
The I-V data stream includes max power point time series for modules in 3 different climate zones up to 6 years of operation. 
The two longest data sets, the BWh and the ET climate zones, were used in this analysis. 
The data includes the max power point and corresponding weather conditions, such as irraidance, ambient temperature, and air mass at that time. 
The data collection interval was inconsistent but occurred every few minutes. 

As both methods use similar data, the cleaning process was the same for both analyses. 
The raw data discussed later will refer to the data set before any cleaning was performed. 
First, the data at $<50\ W/m^2$ irradiance was determined to be nighttime readings and was removed. 
Second, the irradiance readings above 2000 were removed as sensor errors. 
Finally, the power was subset to values only above 1\% of the max power. This filtered out moments when the module was not producing power at high irradiance. 
The ET climate zone showed evidence of snow coverage given by high irradiance with low power and low ambient temperature, so the threshold as changed to capture only power values above 10\% of the max power. 
There were several months at the end of the BWh site that were clear outliers and were removed during processing. 

\section{Results}

Two data sets of max power points from two modules were used for this analysis. 
Each module was located in a different climate zone, ET, and BWh. 

\subsection{BWh Site}
A summary of the BWh site results is shown below.\\ 

\begin{table}
	\caption{Summarized Results from analysis of the BWh climate zone. The multiple values in $R_d$ correspond to each linear segment.\\} 
		\begin{tabular}{c|c|c|c|c}
			Method & $R_d\ (\%/year)$ & Uncertainty & $Adj R^2$ & Raw Data Used \\
			\hline
			MbM & $-0.2,\ -2.1$ & $\pm\ 0.0273$ & $0.21$ &$28\%$ \\
			\hline
			Responsivity & $-0.59$ & $\pm\ 0.40$ & $0.14$ & $2.3\%$ \\
		\end{tabular}
\end{table} \\

The MbM method was able to fit a better predictive model that the responsivity fit while using more of the total data. 
It should be noted that although the MbM has a better $adj-R^2$ both values are lower than what is usually expected. 
This is caused by the seasonality in the data set which creates high variance across the time series. 
A segmented fit was used in the MbM method, with a change points being found at the 39th month. 
This trend shows strong linearity so a linear fit would have also been acceptable with the MbM method. 

The responsivity method shows a similar trend to the MbM method. 
There are similar gaps in the time series where there was missing data. 
It is expected that the methods would have similar results as this is a fairly regular time series with little noise. 
The MbM shows a stronger fit as it has a lower uncertainty and higher $adj R^2$. 
The MbM method also incorporates over 10 times as much of the data during processing. 

\subsection{ET Site}

The second site in the polar climate zone, ET, showed much more variance that the previous site. 
The time series had to be shortened as there was a large amount of missing data at the end. 
There was evidence of snow coverage with this module, given by large amounts of data with high irradiance but low power and a module temperature below or close to zero. 
It can be seen that the segmented fit from the MbM model is able to capture some of the non linearity in this time series, leading to a better fit than the responsivity method, which shows much more noise than the previous site. 
Again, the MbM method shows better fitting statistics and less data removal during processing. 


\begin{table}
	\caption{Summarized Results from analysis of the ET climate zone. The multiple values in $R_d$ correspond to each linear segment.\\} 
	\begin{tabular}{c|c|c|c|c}
		Method & $R_d\ (\%/year)$ & Uncertainty & $Adj R^2$ & Raw Data Used \\
		\hline
		MbM & $-1.0,\ -28,\ -122,\ 119$ & $\pm\ 0.091$ & $0.40$ &$21\%$ \\
		\hline
		Responsivity & $0.19$ & $\pm\ 1.7$ & $0.0014$ & $2.0\%$ \\
	\end{tabular}
\end{table} \\

\subsection{Model Verification}

To check the accuracy of each of the models, power data was pulled from the corresponding to within $\pm2\ W/m^2$ of the standard irradiance used in each model. 
This data is referred to as the measured standard power as it was collected from the raw data, not from a predictive model. 
The power was predicted by each model and compared to the mean measured standard power. 
The more accurate model would yield a prediction closer to the actual data at the same conditions 
The summary of the model comparison is given in the table below. 


For the BWh site, comparisons were done at $36$ months and $57$ months, as they were the available months closest to the requested $3$ and $5$ year comparisons. 
Both models showed strong predictive accuracy,  however the MbM method gave more accurate predictions that the responsivity trend. 
The respective predictions from the MbM method were 83\% and 22\% (relative) closer to the measured standard power at $36$ and $57$ months respectively. 
This is much higher than the requested 5\% relative improvement in the milestone. 
The ET site had a large amount of missing data at the end and data could not be obtained at 5 years, so the comparisons were done at $30$ months and $44$ months (the oldest available). 
The ET site gave similar results, with the MbM method giving relative improvements of 53\% and 85\% over the responsivity method at $30$ and $44$ months respectively. 



 

\section{Conclusion}

A comparison of the MbM method and the NREL responsivity method was done by comparing the results of each model on max power point time series of two modules in two difference climate zones, BWh and ET. 
The MbM method with a segmented linear fit showed improved fitting compared to the assumed linear fit of the responsivity method for both sites. 
A comparison of the accuracy of both models showed that the MbM method gave a relative improvement of $>5\%$ vs the responsivity method in predicting the power at $36$ and $57$ months for the BWh site and $30$ and $44$ months for the ET site. 
All uncertianties for the MbM method trends exclude zero for their fitted time coefficients. 

\end{document}
