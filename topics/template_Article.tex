\documentclass[]{article}

\usepackage{mathtools}
\usepackage{mathpazo}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{amsmath}

%opening
\title{Determining the Power Change Rate of 373 Plant Inverters’ Time-series Data Across Multiple Climate Zones, Using a Month-by-Month Data Science Analysis}
\author{Alan J. Curran, Yang Hu, Rojiar Haddadian, Jennifer L. Braid,\\ David Meakin, Timothy J. Peshek, Roger H. French}

\begin{document}

\maketitle

\begin{abstract}

The month-by-month method has been developed in the Solar Durability and Lifetime Extension (SDLE) Research Center with the purpose of improving the power output monitoring of real world photovoltaic (PV) power plants. 
This method computers the change rate of the system and external variables are ranked based on their correlation with the changing rate of the system to provide insight into the possible degradation mechanisms of PV modules. 
A data set of non-SunPower, front contacted multi-crystalline modules was analyzed with the month-by-month method. 
Results show an average change rate of -0.95 \%/year with an observed correlation between the change rate and the brand of the module, as well as the total age of the system and the climate zone the system is located in.

\end{abstract}

\section{Introduction}

Solar panels are one of the predominant forms or renewable energy and continue to become more popular as the price decreases and the efficiency increases. 
The degradation of solar modules can lead to power loss over time, which can be highly problematic if a photovoltaic (PV) system has been designed and built to produce a certain amount fo power reliably. 
Given the potential risk in a power source that may give diminishing returns over time, it is important to have a strong understanding of how much degradation can be expected when designing a system and in creating modules that are more resistant to degradation. 

Current methods for estimating the degradation rate of a system often involve accelerated testing, which exposes the modules to extreme heat, humidity, and irradiance conditions to induce rapid degradation. 
The expected real-world performance, i.e. the performance that can be expected from a module exposed to outdoor weather conditions over a long period of time, is then extrapolated to a long term estimation from the observed rapid degradation. 
Accelerated testing is important in many cases, including determining the degradation mechanisms and potential stressors, however it cannot be assumed that there will be a strong correlation between accelerated testing and real-world degradation without a proper understanding of the long term performance of real-world PV systems. 
Current methods for modeling the long term performance of PV systems usually involve two significant assumptions. 
The first is the clear sky assumption - that the data should only be considered at close to maximum irradiance with not cloud cover. 
The second is the linear degradation assumption - that module degradation will manifest as a linear change over time. 
In this study, we hope to approach this modeling from a more data-science orientated viewpoint and keep as much data as possible and without making assumptions about the final trend of the degradation.  

\section{Data Sources}

The data used for this study comes from industry sources using front contacted multi-crystalline modules. Using industry sources gives us access to large amounts of data over long periods time of many different modules within many different climate zones. 
The drawback of this is that there many be variable differences between data sets from different companies and the data and sensors many not be as clean or well monitored as an on-site research system. 
To account for this, the Month-by-Month (MbM) method has been designed to be fault tolerant and usable for many different potential variables. 
The idea behind this is that the massive amounts of data we gain access to using industry sources will make-up for the potential decrease in data quality. 

\section{Methodology}

The Month-by-Month (MbM) method is the current change rate analysis process used by the SDLE. 
This method isolates the change rate of a PV system from the variability caused by weather conditions. 
To start, inverter data is segmented into 30 day pseudo-month long sections, and it is assumed that there is no significant degradation occurring within each pseudo-month period. 
For each month long section, a beta ($\beta$) model is created to determine the dependence of the power output on the weather conditions. Once a model is obtained for each month, standard weather conditions are determined over the entire lifetime of the inverter, and each $\beta$ model is evaluated under these standard conditions. 
This yields a predicted power output for each month under standard conditions and the slope and intercept of this plot correlate to the change rate of the system. 
The next model, the Xi ($\xi$) model, determines a fit for the predicted power chart. 
This model can either be linear or piecewise fit, depending on the nature of the predicted power trend. 
Once the $\xi$ model is determined, the yearly change rate can be calculated from the slope of the model. 
The final tool is the gamma ($\gamma$) model, used to compare the metadata across many inverters and determines the most significant variables affecting the change rate. 
For example, the gamma model might show that the change rate of a module is most dependent on the brand. 
Each of the models is explained in more detail below.

\subsection{Data Cleaning}

The data cleaning for this method is designed to be as minimal as possible, only removing data errors and nighttime readings. 
The idea behind this is that there is important system information in more conditions than just the clear sky assumption and we want to capture that. First all missing and interpolated data is removed from the time series. 
Then data with an irradiance of less than $50 W/m^2$ is removed to remove readings during the night. 
Next sensor reading errors are removed, including any irradiance over $2000 W/m^2$ and power readings significantly higher than the nameplate power. 
Finally, readings are removed where the power is less than 1\% of the maximum power to remove any instances when the system was not producing power during the daytime. 
   

\subsection{$\beta$ Pseudo-month Predictive Model}

Each of the month sections has a power dependence based on the ambient weather conditions. 
Standard weather data for inverters includes the ambient temperature, the wind speed, and the irradiance (GHI, POA, etc.). 
The $/beta$ model is the linear relationship of the power to these variables in the form of equation \ref{beta}:

\begin{equation} \label{beta}
P_{AC} = \beta_{0} + \beta_{1} I_{GHI} + \beta_{2} T_{Ambient} + \beta_{3} W_{windspeed} + \epsilon
\end{equation}

Where $P_{AC}$ is the AC power, $\beta_{X}$ are the variable coefficients, and $\epsilon$ is the error of the model. 
Once a model has been created for each month, the standard weather conditions for the inverter lifetime are determined as the overall average temperature, the overall average wind speed, and the minimum of the maximum irradiance values for each month. 
These standard conditions are applied to the $\beta$ model for every month, resulting in a predicted power output for the month. 
This reduces the inverter data to a single predicted power value for each month. Any long term variation in the predicted monthly power output is determined to be due to the degradation of the system as the weather conditions applied to the $\beta$ models are constant.

\subsection{$\xi$ Piecewise Regression Model}

The $\xi$ model determines the fit of the predicted power trend from the $\beta$ model. 
Because each point is derived from a unique $\beta$ model, each point will have a different precision. 
The $\xi$ model uses a weighted least squares analysis to increase the significance of the months that have more precise $\beta$ models and reduce the significance of the months that have less precise $\beta$ models. 
This reduces the influence of monthly predicted power values that might have experienced extreme weather conditions, failure, or maintenance that might have affected the change rate. 
The change rate of the system is defined by the slope and intercept of the $\xi$ model fit in equation \ref{xi}:

\begin{equation} \label{xi}
R_c = 12*\frac{Slope}{Y-intercept}
\end{equation}

The fraction of the slope and intercept of the $\xi$ model is multiplied by $12$ to convert the result to units of $(\% Change/Year)$
This model can also apply a piecewise fit for to plot is non-linearity is observed. 
Even if there was no break in the power production, a change in the string such as a module or inverter replacement might affect the change rate. 
This would be indicated by a piecewise fit in the predicted power plot. 
It is not unusual to see inverters significantly increase their power output in the first few months of operation due to initially incomplete strings or installation maintenance. 
This artificial change rate is easily shown if a predicted plot shows a rapidly increasing piecewise fit in the first few months and can be removed from analysis.

\subsection{$\gamma$ Cross-sectional Model}

The $\gamma$ model is an analysis tool that shows the effect of the metadata variables, such as module brand or climate zone, on the system changing rate. An Akaike Information Criterion (AIC) test is used in the $\gamma$ model to determine which of the variables show greater influence on change rates, indicating a significant effect on the system degradation. Previous analysis of a sample of 655 inverters has shown the Koppen-Geiger climate zone to be the most significant predictor of the change rate.

\subsection{Seasonal Decomposition}

The time series results from the $MbM$ method on this data set show strong seasonality. 
This is most likely due to the irradiance given in Global Horizontal Irradiance (GHI) instead of the more common Plane of Array (POA) irradiance. 
There is a translation error between the measured GHI and the irradiance that actually reaches solar panel, which determines the power output. 
In the winter months, the modules have higher predicted power because the GHI measurement pick up less irradiance than what hits the panels. 
This can be mitigated using seasonal decomposition to remove the seasonal component of the time series, leaving just the trend. 
%TODO Cite stlplus
The $stlplus$ package in R was used to do this. 
Each data point represents one month of time and the seaonality is yearly so the $MbM$ result is defined as a time series with a frequency of $12$ ($12$ months per year) and the seaonality can be significantly reduced. 

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{../figs/1703_sample1_dpa1yun.png}
		\caption{}
		\label{app:trend} 
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\linewidth]{../figs/1703_trend_dpa1yun.png}
		\caption{}
		\label{app:decompose}
	\end{subfigure}
	\caption{(a) Results of the dpa1yun system with seasonality shown.  (b) The dpa1yun system after decomposition. The seasonality have been significantly reduced and the trend is strongly linear.}
\end{figure}

\section{Results}

Of the $373$ inverters in the data set, $353$ were able to be analyzed. 
Several systems had to be excluded due to large amounts of missing or unusable data, including long term interpolated data that had to be removed. 
A few systems failed at the decomposition step; a time series with more than one missing season ($12$ consecutive months in this case) could not be decomposed and were rejected. 
The a histogram of the change rates of each system is given below in figure \ref{fig:hist}. 
The median change rate of this distribution is $-0.98 (\%/year)$ and the mean is $-0.95 (\%/year)$.  

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{../figs/PVSC-histogram}
	\caption{A histogram of the system change rate distribution. The distribution shows strong normality with a few outliers on both sides.} 
	\label{fig:hist}
\end{figure}

Given the shape of the distribution and the similarity of the mean and median, this data set shows strong normality. 
There are several outliers in that can be seen with extreme change rates of $\pm10 (\%year)$.  
These are mostly caused by data or system errors not due to degradation. 
In some cases, the system is not fully monitored or operational or operational at the start of data collection. 
This would cause a large increase in the performance in the first few months as the power output increases as more modules are connected, leading to a high positive change rate if the increase was significant. 
Alternately, module failures can cause sharp drops in the power output as fewer modules are available; this is most often observed as a flat line trend followed by a sharp decrease and return to a flat line trend. 
This is the case in most of the systems with highly negative change rates and may not be due to degradation, but system failures. 
The majority of the systems have a change rate of between about $-3\ to\ 1 (\%/year)$. 
\\ 
After the change rate for each system was found a $\gamma$ cross-sectional model was used to rank the importance of the metadata variables. 
The variables available were the brand and model of the system, the size of the system, the age of the system, and the Koppen-Geiger climate zone the system is located in. 
The module model variable was removed as there were too many different models and too few modules of each model to be comparable. Most models only had one or two associated systems. 
An Akaike Information Criteria was performed using both forwards and backwards regression. 
The metadata variables in order of significance are the modlue brand, the age of the system, and the climate zone the system is located in. 
This data set showed a lesser influence between the system change rate and the climate zone than some other data sets, but it still is statistically significant.    
The distributions for the brand and cliamte zone can be seen below in figures \ref{app:brand} and \ref{app:cz}. 

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[b]{0.55\textwidth}
		\includegraphics[width=\textwidth]{../figs/asy-pvsc-box-brand}
		\caption{}
		\label{app:brand} 
	\end{subfigure}
	\begin{subfigure}[b]{0.55\textwidth}
		\includegraphics[width=\linewidth]{../figs/asy-pvsc-box-cz}
		\caption{}
		\label{app:cz}
	\end{subfigure}
	\caption{(a) Change rate distribution based on the module brand. Different brands show different performance, both in distribution and in the magnitude of the change rate.  (b) Change rate distribution based on the climate zone of the system. There is less difference when compared to the module brand, however there is still a significant difference between some of the climates. }
\end{figure}

\section{Discussion}

The results from the $MbM$ method gave an average yearly change rate of $0.95 \%/year$. 
This is a higher than desired degradation rate, but these systems are fairly old in many cases and may not be as robust as newer modules. There was a failry large distribution of change rates as well, with outliers as far as $\pm 10\%$. 
There are many events that can affect the change rate besides the degradation, such as the addition of more modules or system failures. 
Given the age of these systems, it is not unusual to find faults such as these that affect the modeled change rate but may not be directly caused by degradation. 
The normality of the change rate distribution would suggest that errors like these are distributed evenly throughout and do not heavily skew the data set.
The results of the $\gamma$ model suggest that in this data set the module brand has the most significance on the degradation, which is not unexpected. 
There was also a significant change rate dependence on the age of the system and the climate zone the system was located in. 
Further investigation will need to be done to see what environmental songstress may have influenced the degradation and change rate of the system. 


\end{document}
