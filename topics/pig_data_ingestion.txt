
##step 1: put data in hpc using scp
## step 2: put data from hpc to hdfs using hadoop fs -put (from) (destination)
## step 3 : Run pig command

## pig commands
##Load data from hdfs
A = LOAD 'hdfs://sdlecdh-hn01.priv.cwru.edu:8020/user/mah228/bapvc/0ap0eic.csv' USING PigStorage(',') AS(f1, f2, f3,f4,f5,f6,f7,f8,f9,f10,f11,f12:chararray,f13,f14,f15);

## run a loop so that it can generate year-month rowkey (f12), and variable f1-5,f14,15
d = foreach A generate f12, f1;

## group data for month by month
X = GROUP d BY f12;

## check data using 'dump X'
## it will dump mapreduced output on pig/grunt command shell

## data storing in hbase
## format 'hbase://table_name' USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('columnfamily:file-variablename');

STORE X INTO 'hbase://test2' USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('f1:0ap0eic-Timestamp');


## storing 2nd variable
d = foreach A generate f12, f2;
X = GROUP d BY f12;

STORE X INTO 'hbase://test2' USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('f1:0ap0eic-Ambient_temp');



## storing 3rd variable
d = foreach A generate f12, f3;
X = GROUP d BY f12;

STORE X INTO 'hbase://test2' USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('f1:0ap0eic-Wind_speed');



## storing 4th variable
d = foreach A generate f12, f4;
X = GROUP d BY f12;

STORE X INTO 'hbase://test2' USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('f1:0ap0eic-GHI');



## storing 5th variable
d = foreach A generate f12, f5;
X = GROUP d BY f12;

STORE X INTO 'hbase://test2' USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('f1:0ap0eic-Ac_energy');



## storing 6th variable
d = foreach A generate f12, f14;
X = GROUP d BY f12;

STORE X INTO 'hbase://test2' USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('f1:0ap0eic-AC_power');


####check with hbase scan
##hbase command
#scan 'test2' ,{COLUMNS => ['f1:0ap0eic-Timestamp','f1:0ap0eic-Ambient_temp'], LIMIT =>1} 
#scan 'test2' ,{COLUMNS => 'f1:0ap0eic-Timestamp', LIMIT =>1} 
#scan 'test2' ,{COLUMNS => 'f1:0ap0eic-Ambient_temp', LIMIT =>1} 



